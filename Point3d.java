/**
 * A three-dimensional point class.
 **/
public class Point3d {
    
    /** X coordinate of the point **/
    private double xCoord;
    
    /** Y coordinate of the point **/
    private double yCoord;

    /** Z coordinate of the point **/
    private double zCoord;

    /** Constructor to initialize point to (x, y, z) value. **/
    public Point3d(double x, double y, double z) {
        xCoord = x;
        yCoord = y;
        zCoord = z;
    }

    /** No-argument constructor:  defaults to a point at the origin. **/
    public Point3d() {
        // Call three-argument constructor and specify the origin.
        this(0, 0, 0);
    }

    /** Check is given object equals to this */
    public boolean equals(Point3d p) {
      if (p == null) return false;
      return getX() == p.getX() && getY() == p.getY() && getZ() == p.getZ();
    }

    /** Returns straight-line distance between this and the given point */
    public double distanceTo(Point3d p) {
      return Math.sqrt(
        Math.pow(getX() - p.getX(), 2) +
        Math.pow(getY() - p.getY(), 2) +
        Math.pow(getZ() - p.getZ(), 2)
      );
    }

    /** Return the X coordinate of the point. **/
    public double getX() {
        return xCoord;
    }

    /** Return the Y coordinate of the point. **/
    public double getY() {
        return yCoord;
    }

    /** Set the Z coordinate of the point. **/
    public double getZ() {
        return zCoord;
    }

    /** Set the X coordinate of the point. **/
    public void setX(double val) {
        xCoord = val;
    }

    /** Set the Y coordinate of the point. **/
    public void setY(double val) {
        yCoord = val;
    }

    /** Set the Z coordinate of the point. **/
    public void setZ(double val) {
        zCoord = val;
    }
}