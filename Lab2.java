import java.io.*;
import java.util.*;
import java.nio.file.*;
import java.nio.charset.Charset;

public class Lab2 {
  // Программа считывает из файла in.txt группы по три точки на каждой строке
  // и записывает в файл out.txt площадь между каждой тройкой точек (по одной на строку)
  public static void main(String[] args) {
    Scanner in;
    Path out = Paths.get("out.txt"); // Создаем выходной файл
  
    try {
      in = new Scanner(new File("in.txt")); // Пытаемся открыть входной файл
    } catch (FileNotFoundException e) { // Если не получается выводим сообщение об ошибке
      System.out.println("Не получилось открыть файл in.txt");
      return; // и выходим
    }
    
    // Создаем список строк, которые будем записывать в выходной файл
    ArrayList<String> stroki = new ArrayList<String>();

    while (in.hasNextLine()) { // Будем считывать из файла до тех пор, пока у нем остаются строки
      String stroka = in.nextLine(); // Считываем строку
      Point3d[] tochki = toch(stroka); // Считываем из строки три точки
      double s = ploshad(tochki); // Находим площадь между этими точками
      stroki.add(Double.toString(s)); // Приводим площадь к строке и добавляем в список
    }

    try { // Пытаемся записать строки в выходной файл
      Files.write(out, stroki, Charset.forName("UTF-8"));
    } catch(IOException e) { // Если не получается, выводим сообщение об ошибке
      System.out.println("Не получилось записать out.txt");
    }
  }

  // Считывает из строки три точки
  private static Point3d[] toch(String stroka) {
    // Точки разделены между собой запятыми, разбиваем строку по этому знаку
    String[] tochki = stroka.split(",\\s+");
    double[] coords;
    Point3d[] res = new Point3d[3]; // Создаем массив, который в результате вернем
    for (int i = 0; i < tochki.length; i++) { // Проходимся по всем разбитым точкам
      coords = coords(tochki[i]); // считываем в массив из строки координаты точки
      res[i] = new Point3d(coords[0], coords[1], coords[2]); // создаем новую точку с этими координатами
    }
    return res; // Возвращаем сформированный массив точек
  }

  // Считывает из строки три координаты и возвращает точку
  private static double[] coords(String tochka) {
    // Координаты разделены пробелами, разбиваем по этому символу
    String[] coordsS = tochka.split("\\s+");
    double[] coordsD = new double[3]; // Создаем массив координат, который вернем
    for (int j = 0; j < coordsS.length; j++) { // Проходимся по разбитым строкам
      coordsD[j] = Double.parseDouble(coordsS[j]); // Каждую координату приводим из строки в число
    }
    return coordsD; // Возвращаем сформированный массив координат
  }

  // Находит площадь между тремя точками
  private static double ploshad(Point3d[] tochki) {
    double a = tochki[0].distanceTo(tochki[1]); // Находим длины сторон треугольника
    double b = tochki[1].distanceTo(tochki[2]);
    double c = tochki[2].distanceTo(tochki[0]);
    double p = (a + b + c) / 2; // Находим полупериметр
    double s = Math.sqrt(p * (p - a) * (p - b) * (p - c)); // Вычисляем площадь по формуле Герона
    return s;
  }
}
